import React, { useEffect, useContext } from 'react';
import styles from './Cockpit.css';
import AuthContext from './../../context/auth-context';

const Cockpit = (props) => {

    const assignedClasses = [];
    let btnClass = '';
    const authContext = useContext(AuthContext);   
 
    useEffect(()=>{
        console.log('[Cockpit.js] useEffect');
    })
    
    if(props.showPerson){
        btnClass = styles.red;
    };
    if(props.personsLength <= 2){
        assignedClasses.push( styles.red );
    };
    if(props.personsLength <= 1){
        assignedClasses.push( styles.bold );
    };

    return(
        <div className={styles.Cockpit}>
            <h1>{props.title}</h1>
            <p className={assignedClasses.join(' ')}>This is working</p>
            <button 
             className={btnClass}
             onClick={props.toggle}>
             toggle person
            </button>
               <button onClick={authContext.login}> Login </button>
        </div>
    );
}

export default React.memo(Cockpit);