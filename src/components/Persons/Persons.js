import React, {Component} from "react";
import Person from './Person/Person';

class Persons extends Component{  
   
  // if you want to check all the “this.props” attributes then instead of extending the class with
  // “Component” and implementing “ShouldComponentUpdate()”, Extend the class through 
  // PureComponent. It will automatically implement the “ShouldComponentUpdate()” with all the 
  // “this.props” attributes on the background.
  // For example in the following scienario use “PureComponent”
  // if( nextProps.persons !== this.props.persons ||
  //     nextProps.changed !== this.props.changed ||
  //     nextProps.clicked !== this.props.clicked )   
  shouldComponentUpdate(nextProps, nextState){
    console.log("[persons.js] 1. Should component update");
    if(nextProps.persons !== this.props.persons){
      return true;
    }
    else{
      return false;
    }
  }
  
  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('[Persons.js] 3. getSnapshotBeforeUpdate');
    return { message: 'Snapshot!' };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('[Persons.js] 4. componentDidUpdate');
    console.log(snapshot);
  }

  componentWillUnmount() {
    console.log('[Persons.js] 5. componentWillUnmount');
  }
  
  render(){
    console.log("[persons.js] 2. render");
    return this.props.persons.map((person, index) => {
      return <Person 
        click={() => {this.props.clicked(index)}}
        changed={(event) => {this.props.changed(event, person.id)}}
        name={person.name} 
        age={person.age}
        key={person.id} /> 
      });    
  }
  
}
export default Persons;