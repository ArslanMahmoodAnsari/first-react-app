import React, { Component } from 'react';
import styles from'./Person.css';
import AuthContext from './../../../context/auth-context';



class Person extends Component {
    static contextType = AuthContext;
     
    render(){
        console.log('[Person.js] rendering...');
        return(
            <div className = {styles.Person}>
                { this.context.authenticated ?
                 (<p>Authenticated</p>) : 
                 (<p>Authentication Required....!!!</p>) }
                <p onClick = {this.props.click}>I'm {this.props.name} and  i am {this.props.age} years old</p>
                <p>{this.props.children}</p>
                <input type="Text" onChange={this.props.changed} value={this.props.name}/>
            </div>
        )
    }
      
};

export default Person;