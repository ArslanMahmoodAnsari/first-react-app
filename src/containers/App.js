import React , {Component} from 'react';
import styles from './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from './../components/Cockpit/Cockpit';
import AuthContext from './../context/auth-context';

class App extends Component {
  constructor(props) {
    super(props);
    console.log('[App.js] 1. constructor');
  }

  state = {
    persons:[
      {id: '1', name: 'ali', age: '10'},
      {id: '2', name: 'umer', age: '15'},
      {id: '3', name: 'ayesha', age: '20'}
    ],
    showPerson: false,
    showCockpit: true,
    authenticated: false
  };
  static getDerivedStateFromProps(props, state) {
    console.log('[App.js] 2. getDerivedStateFromProps', props);
    return state;
  }

  componentDidMount() {
    console.log('[App.js] 4. componentDidMount');
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('[App.js] (1) shouldComponentUpdate');
    return true;
  }

  componentDidUpdate() {
    console.log('[App.js] (2) componentDidUpdate');
  }
  togglePersonHandler = () => {
    let show = this.state.showPerson;
    this.setState({showPerson: !show});
  };

  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex((p) => {
      return p.id === id;
    });

    const person =  {
      ...this.state.persons[personIndex]
    };
    
    person.name = event.target.value;
    let persons = [...this.state.persons];
    persons[personIndex] = person; 
    this.setState({persons:persons});
  };

  deletePerson = (personIndex) => {
    let persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons:persons});
  };
  loginHandler = () => {
    this.setState({authenticated:true});
  }

  render(){
    console.log('[App.js] 3. render');
    let persons = null;
    
    if(this.state.showPerson){
      persons = <Persons 
          persons = {this.state.persons}
          clicked = {this.deletePerson}
          changed = {this.nameChangeHandler}/>
    }
   
    return (
        <div className={styles.App}> 
        <button
          onClick={() => {
            this.setState({ showCockpit: false });
          }}> Remove Cockpit
        </button>
        <AuthContext.Provider 
          value={{
            authenticated:this.state.authenticated,
            login:this.loginHandler}}
        >
         {
          this.state.showCockpit ? ( <Cockpit
          title={this.props.appTitle}
          showPerson={this.state.showPerson}
          personsLength={this.state.persons.length}
          toggle={this.togglePersonHandler} />
          ): null
         }
          {persons}  
        </AuthContext.Provider>
        </div>
    )
  };
  
}

export default App;
